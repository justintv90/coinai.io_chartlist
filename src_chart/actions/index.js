export const REQUEST_COIN_DATA = 'DETAIL::REQUEST_COIN_DATA'
export const RECEIVE_COIN_DATA = 'DETAIL::RECEIVE_COIN_DATA'
export const REQUEST_TWEET_DATA = 'DETAIL::REQUEST_TWEET_DATA'
export const RECEIVE_TWEET_DATA = 'DETAIL::RECEIVE_TWEET_DATA'
export const CHANGE_TIMESPAN = 'CATEGORY::CHANGE_TIMESPAN'
export const GET_TIMESPAN = 'CATEGORY::GET_TIMESPAN'


const fetchCoinData = () => (dispatch) => {
  const headers = {
   method: 'GET',
  }
  let symbol = getParamsFromUrl(window.location.href)
// let symbol = 'btc'

  // console.log(window.location.href)
  dispatch(requestCoinData())
  return fetch('https://coinai.io/index.php/Cron/index_new/1?symbol=' + symbol, headers)
  // return fetch('../test_data.json')
    .then(response => response.json())
    .then(json => dispatch(receiveCoinData(json)))
}

const fetchTweetData = () => (dispatch) => {
  const headers = {
   method: 'GET',
    mode: 'cors',
  }
  let symbol = getParamsFromUrl(window.location.href)
  // let symbol = 'btc'

  // console.log(window.location.href)
  dispatch(requestTweetData())
  return fetch('https://social.coinai.io/api/v1/tw/' + symbol, headers)
  // return fetch('../test_data.json')
    .then(response => response.json())
    .then(json => dispatch(receiveTweetData(json.data)))
}

const getParamsFromUrl = url => {
  return url.substr(url.lastIndexOf('/') + 1)
}

const requestCoinData = () => ({
  type: REQUEST_COIN_DATA,
})

const requestTweetData = () => ({
  type: REQUEST_TWEET_DATA,
})

const receiveTweetData = (json) => {
  let totalArray = []
  let uptrendArray = []
  let downtrendArray = []
  Object.keys(json).forEach(k => {
    let value = json[k].value
    let date = new Date(value[0]).getTime()
    let downtrend = value[2]
    let total = value[3]
    let uptrend = value [4]
    totalArray.push([date, total])
    uptrendArray.push([date, uptrend])
    downtrendArray.push([date, downtrend])
  })

  return {
    type: RECEIVE_TWEET_DATA,
    payload: {
      total: totalArray,
      uptrend: uptrendArray,
      downtrend: downtrendArray,
    },
    receiveAt: Date.now()
  } 
}

const receiveCoinData = (json) => {
  // let type = convertObjectIntoArray(json)
  let buyArray = []
  Object.keys(json[1]).forEach(k => {
    let node = []
    node.push(parseFloat(k * 1000))
    node.push(parseFloat(json[1][k]))
    buyArray.push(node)
  })

  let sellArray = []
  Object.keys(json[2]).forEach(k => {
    let node = []
    node.push(parseFloat(k * 1000))
    node.push(parseFloat(json[2][k]))
    sellArray.push(node)
  })

  return {
    type: RECEIVE_COIN_DATA,
    // coin,
    payload: {
      buyVol: buyArray,
      sellVol: sellArray
    },
    receiveAt: Date.now()
  } 
}

const convertObjectIntoArray = obj => {
  let res = [];
  Object.values(obj).forEach(k => {
    res.push(k)
  });
  return res
}

export const fetchCoinDataIfNeeded = time => (dispatch, getState) => {
  if (shouldFetchCoinData(getState(), time)) {
    return dispatch(fetchCoinData())
  }
}

export const fetchTweetDataIfNeed = () => (dispatch, getState) => {
  if (shouldFetchTweetData(getState())) {
    return dispatch(fetchTweetData())
  }
}

export const changeTimeSpanNew = (timeSpan) => {
  return {
    type: CHANGE_TIMESPAN,
    timeSpan
  }
}

const shouldFetchTweetData = (state) => {
  const data = state.social

  if (!data) {
    return true
  }

  if (data.isFetching) {
    return false
  }
  if (data.lastUpdated >= Date.now() - 60000) {
    return true
  } 
  return true 
}

const shouldFetchCoinData = (state, time) => {
  // const posts = state.postsByReddit[reddit]
  const data = state.volume

  if (!data) {
    return true
  }

  if (data.isFetching) {
    return false
  }
  // return state.didInvalidate
  return true
}

const getTimespansFromData = (data) => {
  const buyData = data.buy.percent
  let timeSpans = []
  Object.keys(buyData).forEach(k => {
    timeSpans.push(k)
  })
  return timeSpans
}
