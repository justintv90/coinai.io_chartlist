import React, {Component} from 'react'
import {PropTypes} from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
// import CoinInfoSection from './coins/CoinInfoSection.jsx'
// import CoinTable from './coins/CoinTable.jsx'
//
import Chart from './coins/Chart.jsx'


import {fetchCoinDataIfNeeded, fetchTweetDataIfNeed, changeTimeSpanNew} from '../actions'

class App extends Component {

   componentDidMount() {
      const {dispatch} = this.props
      dispatch(fetchCoinDataIfNeeded(Date.now()))
      this.interval = setInterval( () => {
	 dispatch(fetchCoinDataIfNeeded(Date.now()))
      }, 60000)


   }

   componentWillUnmount() {
      clearInterval(this.interval);
   }

   render() {
      const {buyVol, sellVol, total, uptrend, downtrend} = this.props
      
      let symbol = window.location.href.substr(window.location.href.lastIndexOf('/') + 1)
      if (symbol === 'btc') symbol = 'usdt'.toUpperCase()
      else symbol = 'btc'.toUpperCase()

      let volumeChart = {
	 navigator: {
	    enabled: false
	 },
	 credits: {
	    text: 'CoinAI.io',
	    href: 'http://coinai.io'
	 },
	 rangeSelector: {
	    allButtonsEnabled: true,
	    buttonTheme: { // styles for the buttons
	       fill: 'none',
	       stroke: 'none',
	       'stroke-width': 0,
	       r: 8,
	       style: {
		  color: '#2b3643',
		  fontWeight: 'bold'
	       },
	       states: {
		  hover: {
		  },
		  select: {
		     fill: '#2b3643',
		     style: {
			color: 'white'
		     }
		  }
		  // disabled: { ... }
	       }
	    },
	    selected: 0,
	    buttons: [{
	       type: 'minute',
	       count: 30,
	       text: '1m',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'minute', [1]]]
	       }
	    }, {
	       type: 'hour',
	       count: 3,
	       text: '5m',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'minute', [5]]]
	       }
	    }, {
	       type: 'hour',
	       count: 6,
	       text: '15m',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'minute', [15]]]
	       }
	    }, {
	       type: 'hour',
	       count: 12,
	       text: '30m',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'minute', [30]]]
	       }
	    }, {
	       type: 'day',
	       count: 1,
	       text: '1h',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'hour', [1]]]
	       }
	    }, {
	       type: 'day',
	       count: 4,
	       text: '3h',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'hour', [3]]]
	       }
	    }, {
	       type: 'day',
	       count: 5,
	       text: '6h',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'hour', [6]]]
	       }
	    }, {
	       type: 'month',
	       count: 1,
	       text: '1d',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'day', [1]]]
	       }
	    }, {
	       type: 'month',
	       count: 6,
	       text: '3d',
	       dataGrouping: {
		  forced: true,
		  approximation: 'sum',
		  units: [[ 'day', [3]]]
	       }
	    }]
	 },
	 title: {
	    text: 'Buy & Sell Volume'
	 },
	 tooltip: {
	    style: {
	       width: '200px'
	    },
	    valueDecimals: 4,
	    shared: true,
	    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ' + symbol + '</b><br/>',
	 },
	 yAxis: {
	    title: {
	       text: 'Exchange rate'
	    },
	    position: 'left'
	 },
	 legend: {
	    enabled: true
	 },
	 series: [{
	    name: 'Buy Volume',
	    id: 'buyVol',
	 }, {
	    name: 'Sell Volume',
	    id: 'sellVol'
	 }]
      }


      return (
	 <div>
	    <div className="col-lg-6 col-xs-12 col-sm-12">
	       <div className="portlet light bordered">
		  <div className="portlet-title tabbable-line">
		     <div className="caption">
			<i className="icon-bubbles font-dark hide"></i>
			<span className="caption-subject font-dark bold uppercase">Buy & Sell Volume</span>
		     </div>
		  </div>
		  <div className="portlet-body">
		     <Chart options={volumeChart} 
			container="chart-div" 
			type="stockChart" 
			data={{
			   buyVol,
			   sellVol
			}}
		     /> 
		  </div>
	       </div>
	    </div>





	 </div>
      )
   }
}


const mapStateToProps = (state) => ({
   buyVol: state.volume.buyVol,
   sellVol: state.volume.sellVol,
})


App.propTypes = {
   dispatch: PropTypes.func.isRequired,
   buyVol: PropTypes.array.isRequired,
   sellVol: PropTypes.array.isRequired,
}

export default connect(mapStateToProps)(App)
