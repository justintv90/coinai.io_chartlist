import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Coin extends Component {
	render() {
		const {coin} = this.props;
		return (
			<li>
				<div className="coin_info">
					{coin.name}
					<span className="coin_info-volbuy">
						{coin.buy.percent.m1}
				</span>
					<span className="coin_info-volsell">
						{coin.sell.percent.m1}
				</span>
				</div>
			</li>
		)
	}
}

Coin.propTypes = {
	coin: PropTypes.object.isRequired
}

export default Coin
