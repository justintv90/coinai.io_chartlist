import React, {Component} from 'react'
import {PropTypes} from 'prop-types'
import ReactTable from 'react-table'
import math from 'mathjs'

class CoinTable extends Component {
	render() {
		const {coins, timeSpan, timeSpans, changeTimespan} = this.props
		let buyTimespan = 'buy.volume.' + timeSpan
		let sellTimespan = 'sell.volume.' + timeSpan
		let timespanLabel = timeSpan.split("").reverse().join("").toUpperCase()
		let buyVolumePercentageLabel = 'Vol Buy(' + timespanLabel + ')'
		let sellVolumePercentageLabel = 'Vol Sell(' + timespanLabel + ')'

		const bittrexUri = 'https://bittrex.com/Market/Index?MarketName='

		const timeColumns = timeSpans.map(t => {
			let label = t.split("").reverse().join("").toUpperCase()
			return {
				Header: label,
				id: 'volume' + t,
				accessor: d => d.buy.volume[t],
				sortMethod: (a, b) => (math.compare(a*100000000,b*100000000)),
				Cell: row => {
					let sellPercentage = Math.round(row.original.sell.percent[t])
					let buyPercentage = Math.round(row.original.buy.percent[t])
					return (
						<span>
							<p title={'Buy in ' + label}>
								<span style={{
									color: buyPercentage > 0 ? '#468847'
									: buyPercentage < 0 ? '#b94a48'
									: '#ffbf00',
									transition: 'all .3s ease',
									fontFamily: 'FontAwesome'
								}}
							>{buyPercentage > 0 ? '' : buyPercentage< 0 ? '' : '●'} {buyPercentage}%</span>
						({row.original.buy.volume[t]})
					</p>
					<p title={'Sell in ' + label}>
						<span style={{
							color: sellPercentage > 0 ? '#b94a48'
							: sellPercentage < 0 ? '#468847'
							: '#ffbf00',
							transition: 'all .3s ease',
							fontFamily: 'FontAwesome'
						}}>{sellPercentage > 0 ? '' : sellPercentage< 0 ? '' : '●'} {sellPercentage}%</span>
				({row.original.sell.volume[t]})
			</p>
		</span>
					)

				}

			}
		})

		const buttons = timeSpans.map(i => (
			<button
				key={i}
				onClick={() => changeTimespan(i)}>Click Me {i.split("").reverse().join("").toUpperCase()}</button>
		)) 


		const initColumns = [{
			Header: 'Name',
			id: 'name',
			accessor: d => d.name.toLowerCase(),
			filterable: true,
			filterMethod: (filter, row) => {
				let value = filter.value.toLowerCase()
				return row[filter.id].startsWith(value)
			},
			maxWidth: 150,
			Cell: row => (
				<span>
					{row.value.charAt(0).toUpperCase() + row.value.slice(1).toLowerCase()}
				</span>
			),
			// },  {
			// 	Header: "Volume",
			// 	accessor: sellTimespan,
			// 	sortMethod: (a, b) => (math.compare(a*100000000,b*100000000)),
			// 	maxWidth: 1000,
			// 	Cell: row => {
			// 		let arrayTimespan = Object.keys(row.original.buy.volume)

			// 		return (
			// 			<span>
			// 				{ arrayTimespan.map( t => {

			// 					let sellPercentage = Math.round(row.original.sell.percent[t])
			// 					let buyPercentage = Math.round(row.original.buy.percent[t])

			// let timespanLabel = t.split("").reverse().join("").toUpperCase()
			// 					return (
			// 						<span style={{
			// 							float: "left",
			// 							marginRight: "4px"
			// 						}} key={t}>
			// 						<p style={{
			// 							borderBottom: "1px dotted black"
			// 						}}>
			// 								<span style={{
			// 									fontSize: 13
			// 								}}
			// 						title={'Buy in ' + timespanLabel}	
			// 							>
			// 								<span style={{
			// 									color: buyPercentage > 0 ? '#468847'
			// 									: buyPercentage < 0 ? '#b94a48'
			// 									: '#ffbf00',
			// 									transition: 'all .3s ease',
			// 									fontFamily: 'FontAwesome'
			// 								}}
			// 							>{buyPercentage > 0 ? '' : buyPercentage< 0 ? '' : '●'} {buyPercentage}%</span>
			// 							<span>
			// 								({row.original.buy.volume[t]})</span>
			// 						</span>
			// 					</p>
			// 					<p>
			// 						<span style={{
			// 							fontSize: 13
			// 						}}>
			// 						<span style={{
			// 							color: sellPercentage > 0 ? '#b94a48'
			// 							: sellPercentage < 0 ? '#468847'
			// 							: '#ffbf00',
			// 							transition: 'all .3s ease',
			// 							fontFamily: 'FontAwesome'
			// 						}}>{sellPercentage > 0 ? '' : sellPercentage< 0 ? '' : '●'} {sellPercentage}%</span>
			// 					<span>
			// 						({row.original.sell.volume[t]})</span>
			// 				</span>
			// 			</p>
			// 		</span>
			// 					)
			// 				})}

			// 			</span>

			// 		) 
			// 	}
			// }, {
		}]

		let columns = []

		columns.push.apply(columns,initColumns)
		columns.push.apply(columns,timeColumns)

		columns.push.apply(columns, [{
			Header: 'CoinAI Index',
			maxWidth: 100,
			Cell: row => (
				<span>
					Coming Soon
				</span>
			)
		}, {
			Header: 'Market',
			accessor: 'symbol',
			Cell: row => (
				<a href={row.value !== 'btc' ? bittrexUri + 'BTC-'+ row.value.toUpperCase() : bittrexUri + 'USDT-BTC' } 
					target={'#'}>
					<span style={{
						fontSize: 13
					}}>
					{row.value !== 'btc' ? 'BTC-' + row.value.toUpperCase() : 'USDT-BTC'}
				</span>
			</a>
			),
			maxWidth: 100
		}])


		return (
			<div className="row">
				<ReactTable
					data={coins}
					columns={columns}
					className="-striped -highlight"
				/>
			</div>
		)
	}

}

CoinTable.propTypes = {
	coins: PropTypes.array.isRequired,
	timeSpan: PropTypes.string.isRequired,
	changeTimespan: PropTypes.func.isRequired
}

export default CoinTable
