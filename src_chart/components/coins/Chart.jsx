import React, {Component} from 'react'

import Highcharts from 'highcharts/highstock'

class Chart extends Component {
	componentDidMount() {
		if (this.props.modules) {
			this.props.modules.forEach(function (module) {
				module(Highcharts);
			});
		}
		// Set container which the chart should render to.
		this.chart = new Highcharts[this.props.type || "Chart"](
			this.props.container, 
			this.props.options
		)

	}

	componentWillReceiveProps(nextProps) {
		Object.keys(nextProps.data).forEach(k => {
			this.chart.get(k).setData(nextProps.data[k], true)
		})
	}

	componentWillUnmount() {
		this.chart.destroy()	
	}

	render() {
		return (
			<div id={this.props.container}> </div>
		)
	}
}

export default Chart
