import {REQUEST_COIN_DATA, 
	RECEIVE_COIN_DATA,
	REQUEST_TWEET_DATA,
	RECEIVE_TWEET_DATA,
	CHANGE_TIMESPAN} from '../actions'

const initialState = {
	volume: {
		buyVol: [],
		sellVol: [],
		isFetching: false,
		lastUpdated: Date.now()
	},
	social: {
		total: [],
		downtrend: [],
		uptrend: [],
		isFetching: false,
		lastUpdated: Date.now()	
	},
	isFetching: false,
	didInvalidate: false,
	lastUpdated: Date.now()
}

const coins = (state = initialState, action) => {
	switch (action.type) {
		case REQUEST_COIN_DATA:
			return {
				...state,
				volume: {
					...state.volume,
					isFetching: true
				}
			}
		case RECEIVE_COIN_DATA:
			return {
				...state,
				volume: {
					...state.volume,
					buyVol: action.payload.buyVol,
					sellVol: action.payload.sellVol,
					lastUpdated: action.receiveAt,
				isFetching: false,
				}
			}
		case REQUEST_TWEET_DATA:
			return {
				...state,
				social: {
					...state.social,
					isFetching: true
				}
			}
		case RECEIVE_TWEET_DATA: 
			return {
				...state,
				social: {
					...state.social,
					total: action.payload.total,
					uptrend: action.payload.uptrend,
					downtrend: action.payload.downtrend,
					lastUpdated: action.receiveAt,
					isFetching: false,
				}
			}
		default:
			return state
	}
}

export default coins
