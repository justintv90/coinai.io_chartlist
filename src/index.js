import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import {createStore, applyMiddleware, compose} from 'redux'
import {Provider} from 'react-redux'
import App from './components/App.jsx'
import coins from './reducers'


const middleware = [ thunk ]

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
  // other store enhancers if any
);

const store = createStore(
	coins,
	enhancer
)


ReactDOM.render(
	<Provider store={store}>
		<App />
</Provider>,
	document.getElementById('root')
)

