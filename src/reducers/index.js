import {REQUEST_DATA, RECEIVE_DATA, CHANGE_TIMESPAN} from '../actions'

const initialState = {
	items: [],
	stats: {
		topVolumeBuy: {},
		topVolumeSell: {}
	},
	table: {
		timeSpan: 'm1',
		timeSpans: []
	},
	isFetching: false,
	didInvalidate: false,
	lastUpdated: Date.now()
}

const coins = (state = initialState, action) => {
	switch (action.type) {
		case REQUEST_DATA:
			return {
				...state,
				isFetching: true
			}
		case RECEIVE_DATA:
			return {
				...state,
				isFetching: false,
				items: action.payload.data,
				table: {
					...state.table,
					timeSpans: action.payload.timeSpans
				},
				lastUpdated: action.receiveAt 
			}
		case CHANGE_TIMESPAN:
			return {
				...state,
				table: {
										...state.table,
					timeSpan: action.timeSpan
				}
			}
		default:
			return state
	}
}

export default coins
