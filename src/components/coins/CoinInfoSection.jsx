import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Coin from './Coin.jsx'

class CoinInfoSection extends Component {
	render() {
		const {coins} = this.props
		let topVolumeBuy1DayPercentage = 0
		let topVolumeBuy1Day = 0
		let topVolumeBuy1DayCoin = {}

		coins.forEach( coin => {
			let buyVolume = coin.buy.percent
			if (buyVolume.d1 > topVolumeBuy1Day) {
			topVolumeBuy1Day = buyVolume.d1 
			topVolumeBuy1DayCoin = coin
			}
		})


		return (
			<div className="row">

			</div>

		)
	}
}

CoinInfoSection.propTypes = {
	coins: PropTypes.array.isRequired
}

export default CoinInfoSection
