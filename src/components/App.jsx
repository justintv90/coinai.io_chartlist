import React, {Component} from 'react'
import {PropTypes} from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import CoinInfoSection from './coins/CoinInfoSection.jsx'
import CoinTable from './coins/CoinTable.jsx'


import {fetchCoinDataIfNeeded, changeTimeSpanNew} from '../actions'

class App extends Component {

	componentDidMount() {
		const {dispatch} = this.props
		dispatch(fetchCoinDataIfNeeded(Date.now()))
		this.interval = setInterval( () => {
			dispatch(fetchCoinDataIfNeeded(Date.now()))
		}, 60000)
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}



	render() {
		const {coins, timeSpan, timeSpans, dispatch} = this.props
		return (
			<div>
				<CoinInfoSection coins={coins} />
				<CoinTable 
					coins={coins} 
					timeSpan={timeSpan}
					timeSpans={timeSpans}
					changeTimespan={(a) => dispatch(changeTimeSpanNew(a))}/>
			</div>
		)
	}
}


const mapStateToProps = (state) => ({
	coins: state.items,
	timeSpan: state.table.timeSpan,
	timeSpans: state.table.timeSpans
})


App.propTypes = {
	dispatch: PropTypes.func.isRequired,
	coins: PropTypes.array.isRequired,
	timeSpan: PropTypes.string.isRequired,
	timeSpans: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(App)
