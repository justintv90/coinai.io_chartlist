export const REQUEST_DATA = 'CATEGORY::REQUEST_DATA'
export const RECEIVE_DATA = 'CATEGORY::RECEIVE_DATA'
export const CHANGE_TIMESPAN = 'CATEGORY::CHANGE_TIMESPAN'
export const GET_TIMESPAN = 'CATEGORY::GET_TIMESPAN'


const fetchCoinData = () => (dispatch) => {
	const headers = {
		'Access-Control-Allow-Origin':'*',
		'Content-Type': 'application/json',
	}
	dispatch(requestCoinData())
	return fetch('https://coinai.io/index.php/Cron/all_stat_category/1', { headers })
	// return fetch('../test_data.json')
		.then(response => response.json())
		.then(json => dispatch(receiveCoinData(json)))
}

const requestCoinData = () => ({
	type: REQUEST_DATA,
})

const receiveCoinData = (json) => {
	let res = convertObjectIntoArray(json)
	let timeSpans = getTimespansFromData(res[0])
	return {
		type: RECEIVE_DATA,
		// coin,
		payload: {
			data: res,
			timeSpans: timeSpans
		},
		receiveAt: Date.now()
	} 
}

const convertObjectIntoArray = obj => {
	let res = [];
	Object.values(obj).forEach(k => {
		res.push(k)
	});
	return res
}

export const fetchCoinDataIfNeeded = time => (dispatch, getState) => {
	if (shouldFetchCoinData(getState(), time)) {
		return dispatch(fetchCoinData())
	}
}

export const changeTimeSpanNew = (timeSpan) => {
	console.log(timeSpan)
	return {
	type: CHANGE_TIMESPAN,
	timeSpan
	}

}


const shouldFetchCoinData = (state, time) => {
	// const posts = state.postsByReddit[reddit]
	const data = state.items
	if (!data) {
		return true
	}
	if (data.isFetching) {
		return false
	}
	// return state.didInvalidate
	return time <= Date.now()
}

const getTimespansFromData = (data) => {
	const buyData = data.buy.percent
	let timeSpans = []
	Object.keys(buyData).forEach(k => {
		timeSpans.push(k)
	})
	return timeSpans
}
